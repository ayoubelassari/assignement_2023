package ma.octo.assignement.service;


import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TransferSeviceTest {

    private Transfer transfer;
    private Compte compte1;
    private Compte compte2;

    private Utilisateur utilisateur1;
    private Utilisateur utilisateur2;


    @InjectMocks
    private TransferServiceImp transferService;

    @Mock
    TransferRepository transferRepository;
    @Mock
    AuditService auditService;
    @Mock
    DepositRepository depositRepository;
    @Mock
    CompteRepository compteRepository;


    @BeforeEach
    void initialize(){
        utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("last1");
        utilisateur1.setFirstname("first1");
        utilisateur1.setGender("Male");


        utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("user2");
        utilisateur2.setLastname("last2");
        utilisateur2.setFirstname("first2");
        utilisateur2.setGender("Female");


        compte1 = new Compte();
        compte1.setId(1L);
        compte1.setNrCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(20000));
        compte1.setUtilisateur(utilisateur1);


        compte2 = new Compte();
        compte2.setId(2L);
        compte2.setNrCompte("010000B025001000");
        compte2.setRib("RIB2");
        compte2.setSolde(BigDecimal.valueOf(14000));
        compte2.setUtilisateur(utilisateur2);


        transfer = new Transfer();
        transfer.setMotifTransfer("Rien");
        transfer.setMontantTransfer(BigDecimal.valueOf(2000));
        transfer.setCompteBeneficiaire(compte2);
        transfer.setCompteEmetteur(compte1);
        transfer.setDateExecution(new Date());

    }

    @AfterEach
    void destroy(){
        compte1 =null;
        compte2 =null;

        utilisateur1 =null;
        utilisateur2 =null;
    }

    @DisplayName("l'exception CompteNonExistantException doit etre leve")
    @Test
    public void testTransferSenderAccountNotFoundException(){

        when(compteRepository.findByNrCompte("010000A000001000")).thenReturn(null);

        CompteNonExistantException compteNonExistantException = assertThrows(CompteNonExistantException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(compteNonExistantException.getMessage(),"compte emetteur inexistant");

    }

    @DisplayName("l'exception CompteNonExistantException doit etre leve")
    @Test
    public void testTransferRecieverAccountNotFoundException(){

        when(compteRepository.findByNrCompte("010000A000001000")).thenReturn(compte1);
        when(compteRepository.findByNrCompte("010000B025001000")).thenReturn(null);

        CompteNonExistantException compteNonExistantException = assertThrows(CompteNonExistantException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(compteNonExistantException.getMessage(),"compte recepteur inexistant");

    }

    @DisplayName("l'exception TransactionException doit etre leve")
    @Test
    public void testTransferZeroMoneyException(){
        when(compteRepository.findByNrCompte("010000A000001000")).thenReturn(compte1);
        when(compteRepository.findByNrCompte("010000B025001000")).thenReturn(compte2);

        transfer.setMontantTransfer(BigDecimal.valueOf(0));

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"montant vide");

    }

    @DisplayName("l'exception TransactionException doit etre leve")
    @Test
    public void testTransferInsuffiscientMoneyException(){
        when(compteRepository.findByNrCompte("010000A000001000")).thenReturn(compte1);
        when(compteRepository.findByNrCompte("010000B025001000")).thenReturn(compte2);

        transfer.setMontantTransfer(BigDecimal.valueOf(2));

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"le montant minimale du transfer n est pas atteint");

    }

    @DisplayName("l'exception TransactionExceptiondoit etre leve ")
    @Test
    public void testTransferMaximumMoneyReachedException(){
        when(compteRepository.findByNrCompte("010000A000001000")).thenReturn(compte1);
        when(compteRepository.findByNrCompte("010000B025001000")).thenReturn(compte2);

        transfer.setMontantTransfer(BigDecimal.valueOf(2000000));

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"le montant de transfer maximale est depasse");

    }


    @DisplayName("l'exception InsuffisantException doit etre leve ")
    @Test
    public void testTransferInsuffisantBalanceException(){
        compte1.setSolde(BigDecimal.valueOf(300));

        when(compteRepository.findByNrCompte("010000A000001000")).thenReturn(compte1);
        when(compteRepository.findByNrCompte("010000B025001000")).thenReturn(compte2);

        SoldeDisponibleInsuffisantException transactionException = assertThrows(SoldeDisponibleInsuffisantException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"balance insuffisant");

    }


    @DisplayName("l'exception TransactionException doit etre leve")
    @Test
    public void testTransferTransactionMotifException(){
        when(compteRepository.findByNrCompte("010000A000001000")).thenReturn(compte1);
        when(compteRepository.findByNrCompte("010000B025001000")).thenReturn(compte2);

        transfer.setMotifTransfer(null);

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"motif n est pas valide");

    }


    @DisplayName("le montant de l'argent transféré de l'emetteur doit etre reduit et ajoute au destinataire")
    @Test
    public void testTransferHasBeenSuccessfull(){
        when(compteRepository.findByNrCompte("010000A000001000")).thenReturn(compte1);
        when(compteRepository.findByNrCompte("010000B025001000")).thenReturn(compte2);

        try {
            BigDecimal expectedAccount1Sold = compte1.getSolde().subtract(transfer.getMontantTransfer());
            BigDecimal expectedAccount2Sold = compte2.getSolde().add(transfer.getMontantTransfer());
            transferService.createTransfer(TransferMapper.map(transfer));
            assertEquals(expectedAccount1Sold, compte1.getSolde());
            assertEquals(expectedAccount2Sold, compte2.getSolde());

        } catch (TransactionException e) {
            throw new RuntimeException(e);
        } catch (SoldeDisponibleInsuffisantException e) {
            throw new RuntimeException(e);
        } catch (CompteNonExistantException e) {
            throw new RuntimeException(e);
        }


    }



}