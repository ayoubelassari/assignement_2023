package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DepositServiceTest {

    private Deposit deposit1;
    private Compte compte1;
    private Utilisateur utilisateur1;

    private List<Deposit> depositList;


    @InjectMocks
    private DepositServiceImp depositService;

    @Mock
    AuditService auditService;
    @Mock
    DepositRepository depositRepository;
    @Mock
    CompteRepository compteRepository;



    @BeforeEach
    void initialize(){
        utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("Ayoubelassari1");
        utilisateur1.setLastname("Ayoub1");
        utilisateur1.setFirstname("elassari1");
        utilisateur1.setGender("Male");

        compte1 = new Compte();
        compte1.setId(1L);
        compte1.setNrCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);


        deposit1 = new Deposit();
        deposit1.setMotifDeposit("Rien");
        deposit1.setMontant(BigDecimal.valueOf(2000L));
        deposit1.setCompteBeneficiaire(compte1);
        deposit1.setNom_prenom_emetteur("Ayoub2");
        deposit1.setDateExecution(new Date());



    }

    @AfterEach
    void destroy(){
        compte1 =null;
        utilisateur1 =null;
        deposit1 = null;
    }

    @DisplayName("l'exception CompteNonExistantException doit etre leve")
    @Test
    public void testDepositAccountNotFoundException(){
        when(compteRepository.findByRib(any())).thenReturn(null);

        CompteNonExistantException accountNotFoundException = assertThrows(CompteNonExistantException.class,()->{
            depositService.createDeposit(DepositMapper.map(deposit1));
        });

        assertEquals(accountNotFoundException.getMessage(),"compte inexistant");

    }

    @DisplayName("TransactionException doit etre leve" )
    @Test
    public void testDepositTransactionExceedTransactionMoneyException(){
        when(compteRepository.findByRib(any())).thenReturn(compte1);
        deposit1.setMontant(BigDecimal.valueOf(2000000L));
        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            depositService.createDeposit(DepositMapper.map(deposit1));
        });

        assertEquals(transactionException.getMessage(),"le nombre maximum de transfer est depasse");

    }


    @DisplayName("l'exception TransactionException doit etre leve")
    @Test
    public void testDepositTransactionMotifException(){
        when(compteRepository.findByRib(any())).thenReturn(compte1);

        deposit1.setNom_prenom_emetteur("");

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            depositService.createDeposit(DepositMapper.map(deposit1));
        });

        assertEquals(transactionException.getMessage(),"l'emetteur n'existe pas");

    }

    @DisplayName("le montant du recepteur doit etre incremente")
    @Test
    public void testDepositShouldIncreaseRecieverAmount() throws Exception{
        when(compteRepository.findByRib(any())).thenReturn(compte1);
            BigDecimal account1ExpectedAmount = compte1.getSolde().add(deposit1.getMontant());
            depositService.createDeposit(DepositMapper.map(deposit1));
            assertEquals(account1ExpectedAmount, deposit1.getCompteBeneficiaire().getSolde());

    }
    @DisplayName("Test GetAllDeposits")
    @Test
    public void testDepositShouldGetAllDeposits(){
        Deposit deposit2 = new Deposit();
        deposit2.setMotifDeposit("Rien");
        deposit2.setMontant(BigDecimal.valueOf(2000L));
        deposit2.setCompteBeneficiaire(compte1);
        deposit2.setNom_prenom_emetteur("Ayoub2");
        deposit2.setDateExecution(new Date());

        given(depositService.getAll()).willReturn(List.of(deposit1,deposit2));

        depositList=depositService.getAll();
        Assertions.assertNotEquals(null,depositList);
        Assertions.assertEquals(2,depositList.size());


    }






}
