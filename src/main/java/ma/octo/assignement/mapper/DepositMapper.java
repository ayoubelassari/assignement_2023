package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;

public class DepositMapper {
    private static DepositDto depositDto;

    public static DepositDto map(Deposit deposit) {
        depositDto = new DepositDto();
        depositDto.setCompteBeneficiaire(deposit.getCompteBeneficiaire());
        depositDto.setDate(deposit.getDateExecution());
        depositDto.setMotif(deposit.getMotifDeposit());
        depositDto.setMontant(deposit.getMontant());
        depositDto.setNomPrenomEmetteur(deposit.getNom_prenom_emetteur());


        return depositDto;

    }

    public static Deposit map(DepositDto depositDto, Compte account){
        Deposit deposit = new Deposit();
        deposit.setMotifDeposit(depositDto.getMotif());
        deposit.setMontant(depositDto.getMontant());
        deposit.setCompteBeneficiaire(account);
        deposit.setNom_prenom_emetteur(depositDto.getNomPrenomEmetteur());
        deposit.setDateExecution(depositDto.getDate());
        return deposit;
    }
}
