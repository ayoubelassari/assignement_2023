package ma.octo.assignement.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class TransferDto extends TransactionDto {
  private String nrCompteEmetteur;
  private String nrCompteBeneficiaire;

//  public String getNrCompteEmetteur() {
//    return nrCompteEmetteur;
//  }
//
//  public void setNrCompteEmetteur(String nrCompteEmetteur) {
//    this.nrCompteEmetteur = nrCompteEmetteur;
//  }
//
//  public String getNrCompteBeneficiaire() {
//    return nrCompteBeneficiaire;
//  }
//
//  public void setNrCompteBeneficiaire(String nrCompteBeneficiaire) {
//    this.nrCompteBeneficiaire = nrCompteBeneficiaire;
//  }
//
//  public BigDecimal getMontant() {
//    return montant;
//  }
//
//  public void setMontant(BigDecimal montant) {
//    this.montant = montant;
//  }
//
//  public String getMotif() {
//    return motif;
//  }
//
//  public void setMotif(String motif) {
//    this.motif = motif;
//  }
//
//  public Date getDate() {
//    return date;
//  }
//
//  public void setDate(Date date) {
//    this.date = date;
//  }
}
