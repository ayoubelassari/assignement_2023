package ma.octo.assignement.dto;

import lombok.Data;
import ma.octo.assignement.domain.Compte;

@Data
public class DepositDto extends TransactionDto {
    private String nomPrenomEmetteur;
    private Compte compteBeneficiaire;
}
