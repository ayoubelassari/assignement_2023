package ma.octo.assignement.web;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.DepositService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/deposit")
@AllArgsConstructor
public class DepositController {

    DepositService depositService;

    @GetMapping("/all")
    List<Deposit> getDeposits() {
        return depositService.getAll();
    }


    @PostMapping("/create")
    void createDeposit(@RequestBody DepositDto depositDto) throws CompteNonExistantException, TransactionException {
        depositService.createDeposit(depositDto);
    }
}
