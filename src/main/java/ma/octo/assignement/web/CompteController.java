package ma.octo.assignement.web;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.service.CompteService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/account")
@RestController
@AllArgsConstructor
public class CompteController {

    CompteService compteService;


    @GetMapping("/all")
    List<Compte> getAccounts() {
        return compteService.getCompte();
    }

    @GetMapping("/{id}")
    Compte getAccount(@PathVariable("id") long id) {
        return compteService.getCompte(id);
    }


    @PostMapping("/create")
    Compte createAccount(@RequestBody Compte compte){
        return compteService.save(compte);
    }


    @PutMapping("/update")
    Compte updateAccount(@RequestBody Compte compte) throws CompteNonExistantException {
        return compteService.update(compte);
    }
}

