package ma.octo.assignement.service;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.util.TransactionValidator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Transactional
@Service
@AllArgsConstructor
public class TransferServiceImp implements TransferService{

    AuditService auditService;
    TransferRepository transferRepository;
    CompteRepository compteRepository;
    @Override
    public List<Transfer> getAll() {
        return transferRepository.findAll();
    }

    @Override
    public void createTransfer(TransferDto transferDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        Compte emetteur = compteRepository.findByNrCompte(transferDto.getNrCompteEmetteur());
        Compte recepteur = compteRepository.findByNrCompte(transferDto.getNrCompteBeneficiaire());

        TransactionValidator.validateTransfert(emetteur ,recepteur,transferDto);

        emetteur.setSolde(emetteur.getSolde().subtract(transferDto.getMontant()));

        recepteur.setSolde(new BigDecimal(recepteur.getSolde().intValue() + transferDto.getMontant().intValue()));

        Transfer transfer = TransferMapper.map(transferDto,recepteur,emetteur);

        transferRepository.save(transfer);

        auditService.auditTransfer(transferDto);
    }
}
