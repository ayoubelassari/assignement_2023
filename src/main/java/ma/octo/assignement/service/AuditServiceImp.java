package ma.octo.assignement.service;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.util.EventType;
import ma.octo.assignement.repository.AuditTransferRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuditServiceImp implements AuditService {

    Logger LOGGER = LoggerFactory.getLogger(AuditServiceImp.class);

    @Autowired
    private AuditTransferRepository auditTransferRepository;

    public void auditTransfer(TransferDto transferDto) {
        Audit audit = new Audit();
        audit.setEventType(EventType.TRANSFER);
        audit.setMessage("Transfer from " + transferDto.getNrCompteEmetteur() + " to " + transferDto
                .getNrCompteBeneficiaire() + " for an amount of " + transferDto.getMontant()
                .toString());
        auditTransferRepository.save(audit);
    }


    public void auditDeposit(DepositDto deposit) {
        Audit audit = new Audit();
        audit.setEventType(EventType.DEPOSIT);
        audit.setMessage("Deposit to account " + deposit.getCompteBeneficiaire().getNrCompte() + " by " + deposit
                .getNomPrenomEmetteur() + " for an amount of " + deposit.getMontant()
                .toString());
        auditTransferRepository.save(audit);
    }
}
