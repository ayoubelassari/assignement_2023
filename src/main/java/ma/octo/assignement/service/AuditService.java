package ma.octo.assignement.service;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;

public interface AuditService {
    void auditTransfer(TransferDto transferDto);
    void auditDeposit(DepositDto depositDto);
}
