package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.exceptions.CompteNonExistantException;

import java.util.List;

public interface CompteService {
    List<Compte> getCompte();

    Compte save(Compte compte);

    Compte getCompte(long id);

    Compte update(Compte compte) throws CompteNonExistantException;

}
