package ma.octo.assignement.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.util.TransactionValidator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
@Slf4j
@AllArgsConstructor
public class DepositServiceImp implements DepositService {

    AuditService auditService;
    DepositRepository depositRepository;
    CompteRepository compteRepository;

    @Override
    public void createDeposit(DepositDto depositDto) throws CompteNonExistantException, TransactionException {
        Compte compte = compteRepository.findByRib(depositDto.getCompteBeneficiaire().getRib());
        TransactionValidator.validateDeposit(compte,depositDto);

        compte.setSolde(compte.getSolde().add(depositDto.getMontant()));

        Deposit deposit = DepositMapper.map(depositDto,compte);

        depositRepository.save(deposit);

        auditService.auditDeposit(depositDto);
    }

    @Override
    public List<Deposit> getAll() {
        return depositRepository.findAll();
    }
}
