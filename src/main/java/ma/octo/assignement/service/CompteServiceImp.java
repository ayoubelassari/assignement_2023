package ma.octo.assignement.service;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@AllArgsConstructor
public class CompteServiceImp implements CompteService {

    CompteRepository compteRepository;


    @Override
    public Compte save(Compte compte) {
        return compteRepository.save(compte);
    }

    @Override
    public Compte getCompte(long id) {
        return compteRepository.findById(id).get();
    }

    @Override
    public Compte update(Compte compte) throws CompteNonExistantException {
        Compte compte1 = null;

        if (compte!=null && compte.getId()!=0){
            compte1 = compteRepository.findById(compte.getId()).get();

            if (compte1 != null){
                compte1.setSolde(compte.getSolde()!=null?compte.getSolde():compte1.getSolde());
            }else {
                throw new CompteNonExistantException();
            }
        }else {
            throw new CompteNonExistantException();
        }

        return compte1;

    }


    @Override
    public List<Compte> getCompte() {
        return compteRepository.findAll();
    }
}

