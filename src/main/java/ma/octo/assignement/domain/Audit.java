package ma.octo.assignement.domain;

import lombok.Data;
import ma.octo.assignement.util.EventType;

import javax.persistence.*;

@Entity
@Table(name = "AUDIT")
@Data
public class Audit {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(length = 100)
  private String message;

  @Enumerated(EnumType.STRING)
  private EventType eventType;

//  public Long getId() {
//    return id;
//  }
//
//  public void setId(Long id) {
//    this.id = id;
//  }
//
//  public String getMessage() {
//    return message;
//  }
//
//  public void setMessage(String message) {
//    this.message = message;
//  }
//
//  public EventType getEventType() {
//    return eventType;
//  }
//
//  public void setEventType(EventType eventType) {
//    this.eventType = eventType;
//  }
}
