package ma.octo.assignement.util;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import org.springframework.util.StringUtils;

@Slf4j
public class TransactionValidator {
    private static final int MONTANT_MAXIMAL = 30000;
    private static final int MONTANT_MINIMAL = 50;

    public static void validateTransfert(Compte emetteur, Compte recepteur, TransferDto transferDto)throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

        if (emetteur == null) {
            log.error("compte emetteur inexistant");
            throw new CompteNonExistantException("compte emetteur inexistant");
        }

        if (recepteur == null) {
            log.error("compte recepteur inexistant");
            throw new CompteNonExistantException("compte recepteur inexistant");
        }

        if (transferDto.getMontant() == null || transferDto.getMontant().intValue() == 0) {
            log.error("montant vide");
            throw new TransactionException("montant vide");

        } else if (transferDto.getMontant().intValue() < MONTANT_MINIMAL) {
            log.error("le montant minimale du transfer n est pas atteint");
            throw new TransactionException("le montant minimale du transfer n est pas atteint");


        } else if (transferDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            log.error("le montant de transfer maximale est depasse");
            throw new TransactionException("le montant de transfer maximale est depasse");
        }

        if (transferDto.getMotif() == null || transferDto.getMotif().length() <1) {
            log.error("motif n est pas valide");
            throw new TransactionException("motif n est pas valide");
        }

        if (emetteur.getSolde().intValue() - transferDto.getMontant().intValue() < 0) {
            log.error("Solde de l emetteur insuffisant");
            throw new SoldeDisponibleInsuffisantException();
        }
    }

    public static void validateDeposit(Compte compte, DepositDto depositDto) throws TransactionException,CompteNonExistantException{

        if (compte==null){
            log.warn("compte inexistant");
            throw new CompteNonExistantException();
        }
        if (depositDto.getMontant().intValue() > MONTANT_MAXIMAL){
            log.warn("le nombre maximum de transfer est depasse");
            throw new TransactionException("le nombre maximum de transfer est depasse");
        }

        if (StringUtils.isEmpty(depositDto.getNomPrenomEmetteur())){
            log.warn("l'emetteur n existe pas");
            throw new TransactionException("l'emetteur n'existe pas");
        }

        if (StringUtils.isEmpty(depositDto.getMotif())) {
            log.warn("motif invalid");
            throw new TransactionException("motif invalid");
        }
    }
}
